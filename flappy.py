""" Flappy, game inspired by Flappy Bird.

"""
import turtle
from bird import Bird
from board import Board


def main() -> None:
    bird_size = 10
    bird_speed = 30
    bird_gravity = 6
    bird = Bird(0, 0, bird_size, bird_speed, bird_gravity, True, 'green')
    writer = turtle.Turtle(visible=False)
    score = 0
    new_board = Board(420, 420, 370, bird, writer, score)

    turtle.setup(new_board.length, new_board.width, new_board.position, 0)
    turtle.hideturtle()
    turtle.up()
    turtle.tracer(False)

    new_board.show_score()

    # onscreenclick(new_board.bird.move(0, 30))

    turtle.onkey(lambda: new_board.bird.move(0, bird_speed), 'Up')
    turtle.onkey(lambda: new_board.bird.move(bird_speed, 0), 'Right')
    turtle.onkey(lambda: new_board.bird.move(-bird_speed, 0), 'Left')
    turtle.listen()
    new_board.move()
    turtle.done()


if __name__ == "__main__":
    main()


