from shape import Shape
from ball import Ball
from bird import Bird
from board import Board
from turtle import Turtle


class TestShape:
    new_shape = Shape(0, 0, 5, 10, 'green')

    def test_move(self):
        self.new_shape.x = 0
        self.new_shape.y = 0
        self.new_shape.move(10, 5)
        assert self.new_shape.x == 10
        assert self.new_shape.y == 5


class TestBall:
    new_ball = Ball(0, 0, 5, 5, 'green')


class TestBird:
    new_bird = Bird(0, 0, 10, 15,  5, False, 'green')

    def test_not_alive_bird_is_red(self):
        self.new_bird.alive = False
        assert self.new_bird.get_color() == 'red'

    def test_alive_bird_is_green(self):
        self.new_bird.alive = True
        assert self.new_bird.get_color() == 'green'


class TestBoard:
    new_bird = Bird(0, 0, 10, 15, 5, False, 'green')
    writer = Turtle(visible=False)
    score = 0
    new_board = Board(420, 420, 370, new_bird, writer, score)

    def test_bird_inside_board(self):
        assert self.new_board.inside(self.new_board.bird)
        self.new_board.bird.move(1000, 5)
        assert self.new_board.inside(self.new_board.bird) is False
