from abc import ABC


class Shape(ABC):
    def __init__(self, x, y, size, speed, color) -> None:
        self.x = x
        self.y = y
        self.size = size
        self.speed = speed
        self.color = color

    def move(self, x, y) -> None:
        self.x += x
        self.y += y
