from shape import Shape


class Bird(Shape):
    def __init__(self, x, y, size, speed, gravity, alive, color) -> None:
        super().__init__(x, y, size, speed, color)
        self.gravity = gravity
        self.alive = alive
        self.color = color

    def get_color(self) -> str:
        if self.alive:
            self.color = 'green'
            return self.color
        else:
            self.color = 'red'
            return self.color
