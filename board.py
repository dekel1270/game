import turtle
from ball import Ball
from bird import Bird
from math import sqrt
from random import randrange, choice


MIN_BALL_SPEED = 10
MAX_BALL_SPEED = 30
MIN_BALL_SIZE = 20
MAX_BALL_SIZE = 40


class Board:
    def __init__(self, length, width, position, bird, writer, score) -> None:
        self.length = length
        self.width = width
        self.position = position
        self.bird = Bird(bird.x, bird.y, bird.size, bird.speed, bird.gravity, bird.alive, bird.color)
        self.balls = []
        self.writer = writer
        self.score = score

    def show_score(self) -> None:
        self.writer.color('white')
        self.writer.goto(self.width / 2.5, self.length / 2.5)
        self.writer.color('black')
        self.writer.write(self.score)

    def inside(self, point) -> bool:
        "Return True if point on screen."
        return -self.width / 2.1 < point.x < self.width / 2.1 and -self.width / 2.1 < point.y < self.width / 2.1

    def draw(self) -> None:
        "Draw screen objects."
        turtle.clear()

        turtle.goto(self.bird.x, self.bird.y)

        # draw the bird
        turtle.dot(self.bird.size, self.bird.get_color())

        # draw the balls
        for ball in self.balls:
            turtle.goto(ball.x, ball.y)
            turtle.dot(ball.size, ball.color)

        turtle.update()

    def move(self) -> None:
        self.writer.undo()
        self.writer.write(self.score)
        self.bird.y -= self.bird.gravity

        # make ball go left
        for ball in self.balls:
            ball.x -= ball.speed

        # create ball
        if randrange(10) == 0:
            y = randrange(-self.length / 2.1 - 1, self.length / 2.1 - 1)
            ball = Ball(self.width / 2.1 - 1, y, randrange(MIN_BALL_SIZE, MAX_BALL_SIZE),
                        randrange(MIN_BALL_SPEED, MAX_BALL_SPEED), choice(['green', 'red', 'yellow', 'blue']))
            self.balls.append(ball)

        # update score and remove balls not in game
        while len(self.balls) > 0 and not self.inside(self.balls[0]):
            self.score += 1
            self.balls.pop(0)

        # check if bird in board
        if not self.inside(self.bird):
            self.bird.alive = False
            self.draw()
            return

        # check collision between balls and bird
        for ball in self.balls:
            if sqrt((ball.x - self.bird.x) ** 2 + (ball.y - self.bird.y) ** 2) < ball.size / sqrt(2):
                self.bird.alive = False
                self.draw()
                return

        self.bird.alive = True
        self.draw()

        turtle.ontimer(self.move, 40)

